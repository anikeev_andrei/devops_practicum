variable "cloud_id" {
  type        = string
  description = "virtual cloud id"
  default     = "b1gks9pr4614n8s3qpc5"
  nullable    = false
}

variable "folder_id" {
  type        = string
  description = "id of the folder in cloud"
  default     = "b1gqif0ndbseo6s4i6ek"
  nullable    = false
}

variable "zone" {
  type        = string
  description = "geo zone id"
  default     = "ru-central1-a"
  nullable    = false
}