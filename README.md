В репозитории представлен дипломный проект по курсу "DevOps для эксплуатации и разработки" от Яндекс Практикума.
При выполнении дипломного проекта реализован полный цикл сборки-поставки приложения, c использванием практик CI/CD:
- Исходный код хранится в данном репозитории.
- Написаны пайплайны с описанием шагов сборки бэкенда (Golang) и фронтенда (JavaScript).
- Артефакты сборки (docker-образы) публикуются с версионированием в GitLab Container Registry.
- Написаны Dockerfile'ы для сборки Docker-образов бэкенда и фронтенда.
- Kubernetes-кластер разворачивается в Яндекс Облаке с помощью Terraform.
- Состояние Terraform хранится в S3.
- Небинарные файлы (картинки) хранятся в S3.
- Написаны Helm-чарты для публикации приложения.
- Приложение разворачивается в Kubernetes-кластере с помощью ArgoCD.
- Helm-чарты публикуются и версионируются в Nexus.
- Приложение подключено к системам логирования и мониторинга (Prometheus + Grafana).<br>
<br>
Сайт доступен по адресу momo-store.mooo.info (при работающем kubernetes-кластере).


# СТРУКТУРА ПРОЕКТА

```bash
|
├── backend                       - исходный код backend, Dockerfile, gitlab-ci.yml
├── frontend                      - исходный код Frontend, Dockerfile, gitlab-ci.yml
├── momo-store-chart              - helm-чарты для развертывания приложения momo-store в kubernetes кластере
├── terraform                     - файлы terraform для развертывания kubernetes кластера
├── .gitlab-ci.yml                - корневой pipeline сборки образов backend и frontend
└── .helm-chart.gitlab-ci.yml     - pipeline для загрузки helm-чартов в репозиторий Nexus
```
Готовые образы backend и frontend помещаются в GitLab Container Registry.<br>
Helm-чарты размещаются в Nexus репозитории.<br>
Магазин разваричивается в Kubernetes кластере с помощью ArgoCD.<br>
Установлен и подключен к backend'у Prometheus.<br>
Установлена Grafana.<br>


# ЗАПУСК ПРИЛОЖЕНИЯ

## 0 Подготовительные действия.

### 0.1 Сохраните к себе локально каталог **terraform**.

### 0.2 Создайте в Яндекс.Облаке хранилище для хранения состояния terraform:
*Все сервисы - Object Storage - Создать бакет*

### 0.3 В файле **terraform/s3-backend.tf** укажите имя созданного хранилища:
bucket = "<имя хранилища>"

### 0.4 Создайте Сервисный аккаунт с ролью Editor:
*Все сервисы - Yandex Cloud Console - Сервисные аккаунты - Создать сервисный аккаунт*

### 0.5 Создайте переменные окружения, указав Идентификатор ключа и Секретный ключ от созданного Сервисного аккаунта:
```bash
export ACCESS_KEY="<идентификатор_ключа>"
export SECRET_KEY="<секретный_ключ>"
```

### 0.6 Для инициализации terraform с хранением состояния в S3 выполните команды:
```bash
cd terraform
terraform init -backend-config="access_key=$ACCESS_KEY" -backend-config="secret_key=$SECRET_KEY"
```

## 1. Создание Kubernetes кластер в Яндекс.Облаке

### 1.1
В файле **terraform/main.tf** в секции создания хранилища 
для картинок (yandex_storage_bucket) необходимо поменять наименование бакета:

bucket = "<имя бакета>"

### 1.2
Выполните команды для создания Kubernetes кластера и инфраструктуры:
```bash
cd terraform
terraform plan
terraform apply
```

Узнайте ID вашего kubernetes кластера:

*Дашборд каталога - Managed Service for Kubernetes - Кластеры - mycluster*

## 2. Настройка конфигурации Kubernetes кластер /.kube/config

### 2.1 чтобы получить credentials выполните:
```bash
yc managed-kubernetes cluster get-credentials --id <id_кластера> --external
```
### 2.2 для проверки доступности кластера:
```bash
kubectl cluster-info
```
### 2.3 выполните бэкап текущей конфигурации ./kube/config:
```bash
cp ~/.kube/config ~/.kube/config.bak
```
### 2.4 создайте манифест gitlab-admin-service-account.yaml:

```bash
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin-role
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
```
и примените его:
```bash
kubectl apply -f gitlab-admin-service-account.yaml
```
### 2.5 получите endpoint: публичный ip адрес находится по пути 
*Managed Service for Kubernetes - Кластеры - ваш_кластер - обзор - основное - Публичный IPv4*

### 2.6 получите KUBE_TOKEN:
```bash
kubectl -n kube-system get secrets -o json | jq -r '.items[] | select(.metadata.name | startswith("gitlab-admin")) | .data.token' | base64 --decode
```
### 2.7 настройте конфигурацию:
```bash
export KUBE_URL=https://<см. пункт 5>
export KUBE_TOKEN=<см.пункт 6>
export KUBE_USERNAME=gitlab-admin
export KUBE_CLUSTER_NAME=<id_кластера>

kubectl config set-cluster "$KUBE_CLUSTER_NAME" --server="$KUBE_URL" --insecure-skip-tls-verify=true
kubectl config set-credentials "$KUBE_USERNAME" --token="$KUBE_TOKEN"
kubectl config set-context default --cluster="$KUBE_CLUSTER_NAME" --user="$KUBE_USERNAME"
kubectl config use-context default
```

## 3. Настройка доступа к сервисам из Интернета.

### 3.1 Установите Ingress-контроллер NGINX с менеджером для сертификатов Let's Encrypt:

Установка NGINX Ingress Controller:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.5.1/deploy/static/provider/cloud/deploy.yaml
```

Установка менеджера сертификатов:
  
```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
```

### 3.2 Узнайте IP-адрес Ingress-контроллера

значение EXTERNAL-IP:
```bash
kubectl get svc -n ingress-nginx
```

### 3.3 На сайте https://freedns.afraid.org/subdomain/ создайте субдомен

при создании укажите внешний IP-адрес Ingress-контроллера.

### 3.4
Укажите созданный домен в файле **momo-store-chart\values.yaml**:
```bash
frontend:
  fqdn: <>
```
...
```bash
  ingress:
    rules:
      - host: <>
```

### 3.5 Выпустите сертификат с помощью Certificate Yandex Manager, Let's Encrypt:
https://cloud.yandex.ru/docs/certificate-manager/operations/managed/cert-create?from=int-console-empty-state


## 4. Для возможности скачивания образов из Container Registry в kubernetes необходимо настроить авторизацию:

Создайте секрет:

```bash
kubectl create secret docker-registry docker-config-secret --docker-server=gitlab.praktikum-xxxx.ru:xxxx   --docker-username=<указать_свой_логин>   --docker-password=<указать_свой_пароль>
```
Создайте сервис-аккаунт:

```bash
kubectl create serviceaccount my-serviceaccount
kubectl patch serviceaccount my-serviceaccount -p '{"imagePullSecrets": [{"name": "docker-config-secret"}]}' -n default 
```

## 5. Создайте Nexus-репозиторий для хранения helm-чартов
в Gitlab в настройках CI/CD добавьте переменные:<br>
NEXUS_REPO_USER (логин в Nexus)<br>
NEXUS_REPO_PASS (пароль)<br>
NEXUS_REPO_URL (ссылка на репозиторий)<br>

## 6. Изменение места хранения изображений.

### 6.1.
Сохраните изображения в созданное S3 хранилище (см. пункт 1.1)

### 6.2
Измените пути до файлов изображений в файлах backen'а:
\backend\cmd\api\dependencies\store.go
\backend\cmd\api\app\app_test.go

## 7. Установка и настройка ArgoCD
https://cloud.yandex.ru/docs/managed-kubernetes/operations/applications/argo-cd

### 7.1 Установите ArgoCD
```bash
helm pull oci://cr.yandex/yc-marketplace/yandex-cloud/argo/chart/argo-cd \
  --version 5.4.3-7 \
  --untar && \
helm install \
  --namespace argocd \
  --create-namespace \
  argo-cd ./argo-cd/
```

### 7.2 Настройка ArgoCD
#### 7.2.1 Чтобы узнать временный пароль выполните:

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

#### 7.2.2 Для доступа к ArgoCD выполните:
```bash
kubectl port-forward service/argo-cd-argocd-server -n argocd 8080:443
```
В браузере зайдите на адрес http://localhost:8080<br>
login: admin<br>
password: полученный временный пароль<br>

Смените пароль.

#### 7.2.3 Добавление kubernetes кластера
Не отключая форвардинг портов, проверьте текущий контекст:
```bash
kubectl config current-context
```
залогиньтесь в ArgoCD через CLI:
```bash
argocd login http://localhost:8080
```
Пройдите авторизацию. Добавте кластер:
```bash
argocd cluster add [CONTEXT_NAME]
```
Убедитесь, что кластер добавлен.
```bash
argocd cluster list
```

## 8. Запуск приложения.

В браузере зайдите в ArgoCD по адресу http://localhost:8080

### 8.1 Добавьте репозиторий

*Settings - Repositories - Connect repo*

Choose your connection method: VIA HTTPS<br>
Type: helm<br>
Name: <Придумайте имя><br>
Project: default<br>
Repository URL: <путь до Nexus репозитория><br>
Username: <логин Nexus репозитория><br>
Password: <пароль Nexus репозитория><br>

CONNECT

CONNECTION STATUS должен быть "Successful".

### 8.2 Проверьте настройки Проекта

*Settings - Projects - default*

SCOPED REPOSITORIES: <путь до Nexus репозитория><br>
DESTINATIONS:<br>
  Server: <URL kubernetes кластера><br>
  Namespace: default<br>

### 8.3 Создайте новое приложение

*Application - NEW APP*

Application Name: <Придумайте имя><br>
Project Name: default<br>
Repository URL: <путь до Nexus репозитория><br>
Chart: momo-store<br>
Version: <укажите версию><br>
Cluster URL: <URL kubernetes кластера><br>
Namespace: default<br>

CREATE

После заавершения синхронизации проверьте доступность магазина по доменному адресу.
